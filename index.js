const express = require("express");
const path = require("path");
const ejs = require("ejs");

require("dotenv").config();

const expressPort = process.env.EXPRESS_PORT | 5000;
const viewsPath = path.join(__dirname, "views");

const app = express();

const routers = path.join(__dirname, "routes");

const routerPath = (routerName) => {
  return path.join(routers, routerName, routerName);
};

const funnys = require(routerPath("funnys"));
const root = require(routerPath("_root"));
const other = require(routerPath("_other"));

const ejsOptions = { outputFunctionName: "echo" };

/* allows to set key-value variables within application object
 * some values may include spedial meaning such as:
 * https://stackoverflow.com/questions/25229129/what-app-set-function-does-express-js/44007538#44007538
 *
 * in this case "view engine" has a spceial meaning, and specifies
 * view engine that express will call for app.render under the hood
 */
app.set("view engine", "ejs");

/* tell template engine, which options to use,
 * https://stackoverflow.com/a/76380010
 * https://ejs.co/#docs
 */
app.engine("ejs", (path, data, cb) => {
  ejs.renderFile(path, data, ejsOptions, cb);
});

/* Add some middleware to handle public static files... */
app.use(express.static("public"));

app.use("/", root);
app.use("/funnys", funnys);
app.use("*", other);

app.listen(expressPort, () => {
  console.log(`Express listening on port ${expressPort}`);
});
