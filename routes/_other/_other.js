const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
  res.status(404).render("special/404", { title: "Not found" });
});

module.exports = router;
