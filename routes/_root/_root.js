const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
  /* You can gather this from database or something */
  recentBlogs = [
    {
      id: 1,
      title: "I made this cool website!",
      text: "So i made this website, thought it would be cool idk",
    },
    {
      id: 2,
      title: "Another blogpost!",
      text: "And i made this another blogpost, so website would look silly",
    },
    {
      id: 3,
      title: "Final post... for now...",
      /* text: '<script>alert("Echo does not sanitize inputs!");</script>', */
      text: "Im not the most creative person, don't you know?",
    },
  ];
  /* Searches views directory by default for render targets */
  res.render("index", { title: "Home", recentBlogs });
});

module.exports = router;
